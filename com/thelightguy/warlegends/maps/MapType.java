package com.thelightguy.warlegends.maps;

import java.util.ArrayList;
import java.util.List;

public enum MapType {
    DEFAULT;

    public static List<MapType> types() {
        List<MapType> type = new ArrayList<>();
        type.add(MapType.DEFAULT);
        return type;
    }
}
