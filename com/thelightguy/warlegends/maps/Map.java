package com.thelightguy.warlegends.maps;

import com.thelightguy.warlegends.listeners.BlockBreakRegen;
import com.thelightguy.warlegends.util.Team;
import com.thelightguy.warlegends.util.TeamType;
import org.bukkit.*;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Map {

    private int width;
    private int height;
    private int minplayers;
    private int maxplayers;
    private int maxpalyersperteam;
    private List<Team> teams;
    private List<Location> ores;
    private List<Location> diamonds;
    private List<Location> emeralds;
    private MapType type;
    private int buildLimit;
    private World world;
    private int phase;

    public Map(MapType type) {
        this.type = type;
        if(type == MapType.DEFAULT) {
            World world = Bukkit.getWorld("WarLegends");

            Team spec = new Team(world.getSpawnLocation(),  new ArrayList<UUID>(), null, 0, TeamType.SPECTATOR, "SPECTATOR", "S", DyeColor.GRAY, ChatColor.DARK_GRAY);

            Block b1 = world.getBlockAt(new Location(world, 8,101,15));
            Team t1 = new Team(new Location(Bukkit.getWorld("WarLegends"), 15, 101, 9), new ArrayList<UUID>(), b1, 75, TeamType.DEFAULT, "RED", "R", DyeColor.RED, ChatColor.RED);

            Block b2 = world.getBlockAt(new Location(world, 7,101,-14));
            Team t2 = new Team(new Location(Bukkit.getWorld("WarLegends"), 13, 101, -7), new ArrayList<UUID>(), b2, 75, TeamType.DEFAULT, "BLUE", "B", DyeColor.BLUE, ChatColor.BLUE);

            Block b3 = world.getBlockAt(new Location(world, 36,101,-15));
            Team t3 = new Team(new Location(Bukkit.getWorld("WarLegends"), 29, 101, -9), new ArrayList<UUID>(), b3, 75, TeamType.DEFAULT, "GREEN", "G", DyeColor.LIME, ChatColor.GREEN);

            Block b4 = world.getBlockAt(new Location(world, 37,101,14));
            Team t4 = new Team(new Location(Bukkit.getWorld("WarLegends"), 31, 101, 7), new ArrayList<UUID>(), b4, 75, TeamType.DEFAULT, "YELLOW", "Y", DyeColor.YELLOW, ChatColor.YELLOW);
            teams = new ArrayList<Team>();
            emeralds = new ArrayList<Location>();
            diamonds = new ArrayList<Location>();
            ores = new ArrayList<Location>();
            teams.add(spec);
            teams.add(t1);
            teams.add(t2);
            teams.add(t3);
            teams.add(t4);
            width = 500;
            height = 500;
            buildLimit = 3;
            phase = 0;
            minplayers = 2;
            maxplayers = teams.size()*4;
            this.world = world;

            /*
             * EMERALD
             */
            ores.add(new Location(world, 15, 99, -1));
            ores.add(new Location(world, 15, 99, 0));
            ores.add(new Location(world, 14, 99, 1));
            ores.add(new Location(world, 29, 99, -1));
            ores.add(new Location(world, 30, 99, -1));
            ores.add(new Location(world, 31, 99, 0));

            emeralds.add(new Location(world, 15, 99, -1));
            emeralds.add(new Location(world, 15, 99, 0));
            emeralds.add(new Location(world, 14, 99, 1));
            emeralds.add(new Location(world, 29, 99, -1));
            emeralds.add(new Location(world, 30, 99, -1));
            emeralds.add(new Location(world, 31, 99, 0));


            /*
             * REDSTONE
             */
            ores.add(new Location(world, 22, 99, 10));
            ores.add(new Location(world, 21, 99, 9));
            ores.add(new Location(world, 22, 99, 7));
            ores.add(new Location(world, 21, 99, -8));
            ores.add(new Location(world, 22, 99, -7));
            ores.add(new Location(world, 22, 99, -5));

            /*
             * COAL
             */
            //RED
            ores.add(new Location(world, 20, 100, 20));
            ores.add(new Location(world, 17, 99, 17));
            ores.add(new Location(world, 15, 99, 17));
            ores.add(new Location(world, 16, 99, 15));
            ores.add(new Location(world, 14, 102, 20));
            ores.add(new Location(world, 15, 101, 19));

            //BLUE
            ores.add(new Location(world, 7, 99, -6));
            ores.add(new Location(world, 5, 99, -5));
            ores.add(new Location(world, 5, 99, -7));
            ores.add(new Location(world, 3, 101, -7));
            ores.add(new Location(world, 2, 102, -8));

            //GREEN
            ores.add(new Location(world, 28, 99, -15));
            ores.add(new Location(world, 27, 99, -17));
            ores.add(new Location(world, 29, 99, -17));
            ores.add(new Location(world, 29, 101, -19));
            ores.add(new Location(world, 30, 102, -20));

            //YELLOW
            ores.add(new Location(world, 37, 99, 6));
            ores.add(new Location(world, 39, 99, 7));
            ores.add(new Location(world, 39, 99, 5));
            ores.add(new Location(world, 42, 102, 8));
            ores.add(new Location(world, 41, 101, 7));

            /*
             * IRON
             */
            //RED
            ores.add(new Location(world, 16, 100, 18));
            ores.add(new Location(world, 18, 99, 18));
            ores.add(new Location(world, 16, 102, 19));
            ores.add(new Location(world, 17, 101, 19));

            //Blue
            ores.add(new Location(world, 4, 99, -4));
            ores.add(new Location(world, 4, 100, -6));
            ores.add(new Location(world, 3, 101, -5));
            ores.add(new Location(world, 3, 102, -6));

            //GREEN
            ores.add(new Location(world, 28, 102, -19));
            ores.add(new Location(world, 27, 101, -19));
            ores.add(new Location(world, 28, 100, -18));
            ores.add(new Location(world, 29, 99, -16));

            //YELLOW
            ores.add(new Location(world, 40, 100, 6));
            ores.add(new Location(world, 38, 99, 7));
            ores.add(new Location(world, 40, 99, 4));
            ores.add(new Location(world, 41, 101, 5));

            /*
             * GOLD
             */

            //RED
            ores.add(new Location(world, 1, 101, 13));
            ores.add(new Location(world, 2, 100, 13));
            ores.add(new Location(world, 1, 99, 11));
            ores.add(new Location(world, 1, 101, 10));

            //BLUE
            ores.add(new Location(world, 9, 101, -21));
            ores.add(new Location(world, 9, 100, -20));
            ores.add(new Location(world, 11, 99, -21));
            ores.add(new Location(world, 12, 100, -21));

            //GREEN
            ores.add(new Location(world, 43, 101, -13));
            ores.add(new Location(world, 42, 100, -13));
            ores.add(new Location(world, 43, 99, -11));
            ores.add(new Location(world, 43, 100, -10));

            //YELLOW
            ores.add(new Location(world, 35, 101, 21));
            ores.add(new Location(world, 35, 100, 20));
            ores.add(new Location(world, 33, 99, 21));
            ores.add(new Location(world, 32, 100, 21));

            /*
             * DIAMONDS
             */
            ores.add(new Location(world, 19, 99, 2));
            ores.add(new Location(world, 19, 99, -1));
            ores.add(new Location(world, 20, 98, 0));
            ores.add(new Location(world, 21, 98, 1));
            ores.add(new Location(world, 22, 99, 4));
            ores.add(new Location(world, 23, 99, 3));
            ores.add(new Location(world, 23, 98, -1));
            ores.add(new Location(world, 25, 98, 0));
            ores.add(new Location(world, 26, 99, 0));
            ores.add(new Location(world, 24, 99, -3));

            diamonds.add(new Location(world, 19, 99, 2));
            diamonds.add(new Location(world, 19, 99, -1));
            diamonds.add(new Location(world, 20, 98, 0));
            diamonds.add(new Location(world, 21, 98, 1));
            diamonds.add(new Location(world, 22, 99, 4));
            diamonds.add(new Location(world, 23, 99, 3));
            diamonds.add(new Location(world, 23, 98, -1));
            diamonds.add(new Location(world, 25, 98, 0));
            diamonds.add(new Location(world, 26, 99, 0));
            diamonds.add(new Location(world, 24, 99, -3));


        }
    }
    public void nextPhase() {
        this.phase++;
        Bukkit.broadcastMessage("§c§l>> §7Phase "+phase+" has arrived");
        if(phase >= 2) {
            for(Location loc : emeralds) {
                loc.getBlock().setType(Material.EMERALD_ORE);
                BlockBreakRegen.nobreak.remove(loc);
            }
        }
        if(phase >= 3) {
            for(Location loc : diamonds) {
                loc.getBlock().setType(Material.DIAMOND_ORE);
                BlockBreakRegen.nobreak.remove(loc);
            }
        }
    }
    public int getPhase() {
        return phase;
    }
    public int getBuildLimit() {
        return buildLimit;
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public List<Team> getTeams() {
        return teams;
    }
    public MapType getType() {
        return type;
    }
    public int getMinplayers() {
        return minplayers;
    }
    public int getMaxplayers() {
        return maxplayers;
    }
    public int getMaxpalyersperteam() {
        return maxpalyersperteam;
    }
    public World getWorld() {
        return world;
    }
    public List<Location> getOres() {
        return ores;
    }
    public List<Location> getDiamonds() {
        return diamonds;
    }
    public List<Location> getEmeralds() {
        return emeralds;
    }
}
