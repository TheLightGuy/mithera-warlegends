package com.thelightguy.warlegends.commands;

import com.thelightguy.mith.main.api.API;
import com.thelightguy.mith.main.api.player.MithPlayer;
import com.thelightguy.warlegends.main;
import com.thelightguy.warlegends.util.Team;
import com.thelightguy.warlegends.util.TeamType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Emile on 29-6-2017.
 */
public class team implements CommandExecutor, Listener {


    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if(e.getInventory().getName() == null) return;
        if(!e.getInventory().getName().equals(inv().getName())) return;
        boolean found = false;
        if(e.getCurrentItem() == null) return;
        if(!e.getCurrentItem().hasItemMeta()) return;
        if(!e.getCurrentItem().getItemMeta().hasDisplayName()) return;
        for(Team t : main.map.getTeams()) {
            if(found) continue;
            if(e.getCurrentItem().getItemMeta().getDisplayName().equals(t.getChatColor()+"§l"+t.getName())) {
                Player p = (Player) e.getWhoClicked();
                double teams = main.map.getTeams().size()-1;
                double players = Bukkit.getOnlinePlayers().size()-main.map.getTeams().get(0).getPlayers().size();
                double tpl = t.getPlayers().size();
                double tp = players/teams;
                if(tp>=0.5 && tp < 1.1) {
                    tp =1;
                } else {
                    tp= Math.rint(tp);
                }
                if(t.getPlayers().contains(p.getUniqueId())) {
                    p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1,1);
                    p.closeInventory();
                    p.sendMessage("§cYou can't join this team");
                    return;
                }
                if(tp > tpl || t.getTeamType() == TeamType.SPECTATOR) {
                    if(main.map.getPhase() < 3) {
                        if(main.map.getPhase() != 0) {
                            if(p.hasPermission("mithera.warlegends.joinwhenstarted")) {
                                p.sendMessage("§aYou joined "+t.getChatColor()+"§l"+t.getName());
                                t.addPlayer(p);
                                t.spawnPlayer(p);
                            } else {
                                p.sendMessage("§cYou must have Mithecal ");
                                p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1,1);
                            }
                        } else {
                            p.sendMessage("§aYou joined "+t.getChatColor()+"§l"+t.getName());
                            t.addPlayer(p);
                        }
                        for(Player pl : Bukkit.getOnlinePlayers()) {
                            if(pl.getOpenInventory().getTopInventory().getName().equals(inv().getName())) {
                                pl.getOpenInventory().getTopInventory().setContents(inv().getContents());
                                pl.updateInventory();
                            }
                        }
                        p.closeInventory();
                        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1,1);
                    } else {
                        p.sendMessage("§cYou can't join a team anymore");
                        p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1,1);
                    }
                } else {
                    p.sendMessage("§cYou can't join this team, team balancing");
                    p.playSound(p.getLocation(), Sound.ENDERMAN_TELEPORT, 1,1);
                }
                found = true;
            }
        }
        e.setCancelled(true);
    }


    public static Inventory inv() {
        Inventory inv = Bukkit.createInventory(null, 9*3, "§c§lWARLEGENDS §rTeamselector");
        int i = 0;
        for(Team t : main.map.getTeams()) {
            ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, t.getColor().getData());
            ItemMeta meta = item.getItemMeta();
            item.setAmount(t.getPlayers().size());
            List<String> lore = new ArrayList<String>();
            lore.add("§7Players:");
            int in = 0;
            for(UUID id : t.getPlayers()) {
                if(in > 10) continue;
                Player pl = Bukkit.getPlayer(id);
                MithPlayer p = new MithPlayer(pl);
                if(p.isNick()) {
                    lore.add("§7- "+p.getNickgroup().getColor()+p.getNickname());
                } else {
                    lore.add("§7- "+p.getGroup().getColor()+p.getNickname());
                }
                in++;
            }
            lore.add(" ");
            if(!t.isNexusDead()) {
                meta.setDisplayName(t.getChatColor()+"§l"+t.getName());
                lore.add("§a§lClick §7to join");
            } else {
                meta.setDisplayName(t.getChatColor()+"§m§l"+t.getName());
                lore.add("§c§lDEAD");
            }
            meta.setLore(lore);
            item.setItemMeta(meta);
            if(t.getTeamType() != TeamType.SPECTATOR) {
                inv.setItem(10+i,item);
                i++;
            } else {
                inv.setItem(26,item);
            }
        }
        return inv;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(sender instanceof Player) {
            if(args.length == 0) {
                Player p = (Player) sender;
                p.openInventory(inv());
            }
        }
        return true;
    }
}
