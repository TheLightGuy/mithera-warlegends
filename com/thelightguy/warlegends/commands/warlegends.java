package com.thelightguy.warlegends.commands;

import com.thelightguy.warlegends.main;
import com.thelightguy.warlegends.maps.Map;
import com.thelightguy.warlegends.maps.MapType;
import com.thelightguy.warlegends.util.Game;
import com.thelightguy.warlegends.util.Team;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class warlegends implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
        if(args.length == 0) {

            return true;
        } else {
            if(args[0].equalsIgnoreCase("start")) {
                Game.start();
                sender.sendMessage("Game starting");
                return true;
            }  else if(args[0].equalsIgnoreCase("addphase")) {
                main.map.nextPhase();
                sender.sendMessage("adding a phase");
                return true;
            } else if(args[0].equalsIgnoreCase("timer")) {
                Game.timer = 10;
                sender.sendMessage("skippedtimer");
                return true;
            } else if(args[0].equalsIgnoreCase("killallnexus")) {
                for(Team t : main.map.getTeams()) {
                    t.setNexusHealth(0);
                }
                sender.sendMessage("Killed all nexusus");
                return true;
            } else if(args[0].equalsIgnoreCase("stop")) {
                Game.stop();
                sender.sendMessage("Game stopped");
                return true;
            } else if(args[0].equalsIgnoreCase("setmap")) {
                if(args[1].isEmpty()) {
                    sender.sendMessage("usage: /wl setmap (map)");
                    return true;
                } else {
                    if(main.isStarted) {
                        sender.sendMessage("game already started");
                        return true;
                    }
                    if(args[1].equalsIgnoreCase("default")) {
                        main.map = new Map(MapType.DEFAULT);
                        return true;
                    } else {
                        sender.sendMessage("map not found");
                        return true;
                    }
                }
            } else {
                sender.sendMessage("command not found :/");
                return true;
            }
        }
    }
}
