package com.thelightguy.warlegends;

import com.thelightguy.warlegends.commands.team;
import com.thelightguy.warlegends.commands.warlegends;
import com.thelightguy.warlegends.listeners.*;
import com.thelightguy.warlegends.maps.Map;
import com.thelightguy.warlegends.maps.MapType;
import com.thelightguy.warlegends.util.Game;
import org.bukkit.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class main extends JavaPlugin {

    public static Plugin plugin;
    public static boolean isStarted;
    public static boolean isEnded = false;

    public static Map map;

    public static List<UUID> spectators = new ArrayList<UUID>();
    public static List<UUID> ingame = new ArrayList<UUID>();

    @Override
    public void onEnable() {
        plugin = this;

        /*
         * Listeners
         */
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BlockBreakPlace(), this);
        pm.registerEvents(new PlayerJoinLeave(), this);
        pm.registerEvents(new PlayerDeathEvent(), this);
        pm.registerEvents(new NexusDefense(), this);
        pm.registerEvents(new team(), this);
        pm.registerEvents(new BlockBreakRegen(), this);

        /*
         * Commands
         */
        getCommand("warlegends").setExecutor(new warlegends());
        getCommand("team").setExecutor(new team());

        /*
         * Construcors
         */
        Game.loopCloud();
        Game.loop();
        getServer().createWorld(new WorldCreator("WarLegends"));
        Random r = new Random();
        int size = MapType.types().size();
        int m = r.nextInt(size);
        map = new Map(MapType.types().get(m));
        Game.timer();
        for(Location loc : map.getDiamonds()) {
            BlockBreakRegen.nobreak.add(loc);
            loc.getBlock().setType(Material.STAINED_CLAY);
            loc.getBlock().setData(DyeColor.BLUE.getData());
        }
        for(Location loc : map.getEmeralds()) {
            BlockBreakRegen.nobreak.add(loc);
            loc.getBlock().setType(Material.STAINED_CLAY);
            loc.getBlock().setData(DyeColor.LIME.getData());
        }
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    @Override
    public void onDisable() {

    }

}
