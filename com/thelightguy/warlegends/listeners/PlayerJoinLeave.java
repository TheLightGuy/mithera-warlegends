package com.thelightguy.warlegends.listeners;

import com.thelightguy.mith.main.api.API;
import com.thelightguy.mith.main.api.player.MithPlayer;
import com.thelightguy.warlegends.main;
import com.thelightguy.warlegends.util.Game;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinLeave implements Listener {

    @EventHandler
    public void onPlayerPre(PlayerLoginEvent e) {
        if(main.isEnded) {
            e.setKickMessage("This server is restarting");
        }
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        if(main.isStarted) {
            main.map.getTeams().get(0).addPlayer(e.getPlayer());
        } else {
            MithPlayer mp = new MithPlayer(e.getPlayer());
            if(mp.isVanish()) {
                e.setJoinMessage("");
                return;
            }
            int p = Bukkit.getOnlinePlayers().size();
            int sp = main.map.getTeams().get(0).getPlayers().size();
            int v = API.vanishedPlayers;
            int pls = p-sp-v;
            if(mp.isNick()) {
                e.setJoinMessage("§c§l>> "+mp.getNickgroup().getColor()+mp.getNickname()+" §7joined the game. §c("+pls+"/"+main.map.getMaxplayers()+")");
            } else {
                e.setJoinMessage("§c§l>> "+mp.getGroup().getColor()+mp.getNickname()+" §7joined the game. §c("+pls+"/"+main.map.getMaxplayers()+")");
            }
        }
    }

    @EventHandler
    public void onPlayerLeaveEvent(PlayerQuitEvent e) {
        if(main.isStarted) {
            e.setQuitMessage("");
        } else {
            int p = Bukkit.getOnlinePlayers().size();
            int sp = main.map.getTeams().get(0).getPlayers().size();
            int v = API.vanishedPlayers;
            int pls = p-sp-v;
            MithPlayer mp = new MithPlayer(e.getPlayer());
            if(mp.isVanish()) {
                e.setQuitMessage("");
                return;
            }
            if(mp.isNick()) {
                e.setQuitMessage("§c§l>> "+mp.getNickgroup().getColor()+mp.getNickname()+" §7left the game. §c("+pls+"/"+main.map.getMaxplayers()+")");
            } else {
                e.setQuitMessage("§c§l>> "+mp.getGroup().getColor()+mp.getNickname()+" §7left the game. §c("+pls+"/"+main.map.getMaxplayers()+")");

            }
        }
    }
}
