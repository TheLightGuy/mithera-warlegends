package com.thelightguy.warlegends.listeners;

import com.thelightguy.mith.main.api.player.MithPlayer;
import com.thelightguy.mith.main.api.Util;
import com.thelightguy.warlegends.main;
import com.thelightguy.warlegends.util.Game;
import com.thelightguy.warlegends.util.Team;
import com.thelightguy.warlegends.util.TeamType;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class PlayerDeathEvent implements Listener{

    @EventHandler
    public void event(org.bukkit.event.entity.PlayerDeathEvent e) {
        e.getEntity().setHealth(20);
        e.getEntity().setFoodLevel(20);
        e.getEntity().setFireTicks(0);
        if(!main.isStarted) {
            return;
        }
        final Player p = e.getEntity();
        MithPlayer mp = new MithPlayer(p);
        for(ItemStack i :e.getDrops()) {
            if(!i.hasItemMeta()) continue;
            if(!i.getItemMeta().hasLore()) continue;
            if(i.getItemMeta().getLore().contains("§cSoulbound")) {
                e.getDrops().remove(i);
            }
        }

        Team tm = main.map.getTeams().get(0);
        Team ttm = main.map.getTeams().get(0);
        for(Team t : main.map.getTeams()) {
            if(t.getPlayers().contains(e.getEntity().getUniqueId())) {
                tm = t;
            }
            if(e.getEntity().getKiller() != null) {
                if(t.getPlayers().contains(e.getEntity().getKiller().getUniqueId())) {
                    ttm = t;
                }
            }
        }
        String suffix = "";
        if(tm.isNexusDead()) {
            suffix = " §c§lFINAL KILL";
        }
        e.getEntity().setGameMode(GameMode.SPECTATOR);
        e.getEntity().setVelocity(new Vector(0,0,0));
        if(e.getEntity().getKiller() != null) {
            MithPlayer mpp = new MithPlayer(e.getEntity().getKiller());
            Util.respawntitle(p);
            e.setDeathMessage(tm.getChatColor()+mp.getNickname() + "§7 was killed by " + ttm.getChatColor() + mpp.getNickname() + suffix);
        } else {
            e.setDeathMessage(tm.getChatColor()+mp.getNickname() + "§7 died " + suffix);
        }
        if(tm.isNexusDead()) main.map.getTeams().get(0).addPlayer(p);
        if(Game.checkTeam()) return;
        final Team ptm = tm;
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                Util.respawnsubtitle(p, 3);
                Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        Util.respawnsubtitle(p, 2);
                        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                Util.respawnsubtitle(p, 1);
                                Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
                                    @Override
                                    public void run() {
                                        Util.respawnedtitle(p);
                                        p.setGameMode(GameMode.SURVIVAL);
                                        ptm.spawnPlayer(p);
                                    }
                                },20);
                            }
                        },20);
                    }
                },20);
            }
        },20);
    }
}
