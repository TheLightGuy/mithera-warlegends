package com.thelightguy.warlegends.listeners;

import com.thelightguy.mith.main.api.API;
import com.thelightguy.mith.main.api.particle.ParticleEffect;
import com.thelightguy.warlegends.main;
import com.thelightguy.warlegends.util.Team;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NexusDefense implements Listener {

    public static HashMap<Fireball, Team> arrows = new HashMap<Fireball, Team>();
    public static List<Fireball> arrowList = new ArrayList<Fireball>();


    public static synchronized void handle() {
        new BukkitRunnable() {
            public void run() {
                for(Fireball a : arrowList) {
                    if(a.isDead() || a.isOnGround()) {
                        a.remove();
                        continue;
                    }
                    Team t = arrows.get(a);
                    DyeColor color = t.getColor();
                    int r = color.getColor().getRed();
                    int g = color.getColor().getGreen();
                    int b = color.getColor().getBlue();
                    for(int i = 0; i < 41; i++) {
                        ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(r,g,b), a.getLocation(), 100);
                        ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(r,g,b), a.getLocation(), 100);
                        ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(r,g,b), a.getLocation(), 100);
                    }
                }
            }
        }.runTaskTimer(main.plugin, 1L, 1L);
    }

    @EventHandler
    public void explode(EntityExplodeEvent e) {
        if(e.getEntity() instanceof Fireball) {
            Fireball f = (Fireball) e.getEntity();
            if(arrowList.contains(f)) {
                e.setCancelled(true);
                Team t = arrows.get(f);
                for(Entity en : f.getNearbyEntities(2,2,2)) {
                    if(!(en instanceof Player)) continue;
                    Player p = (Player) en;
                    if(t.getPlayers().contains(p.getUniqueId())) continue;
                    p.damage(0.5);

                }
            }
        }
    }

    @EventHandler
    public void hit(EntityDamageByEntityEvent e) {
        if(e.getDamager() instanceof Fireball) {
            Fireball a = (Fireball) e.getDamager();
            if(arrows.containsKey(a)) {
                if(e.getEntity() instanceof Player) {
                    Player p = (Player) e.getEntity();
                    Team t = arrows.get(a);
                    if(t.getPlayers().contains(p.getUniqueId())) {
                        e.setCancelled(true);
                    } else {
                        e.setCancelled(true);
                        p.damage(0.5);
                    }
                }
                a.remove();
            }
        }
    }
}
