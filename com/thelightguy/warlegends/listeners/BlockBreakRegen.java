package com.thelightguy.warlegends.listeners;

import com.thelightguy.warlegends.main;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class BlockBreakRegen implements Listener {

    public static List<Location> nobreak = new ArrayList<Location>();

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if(nobreak.contains(e.getBlock().getLocation())) {
            e.setCancelled(true);
        }
        if(main.map.getOres().contains(e.getBlock().getLocation())) {
            if(e.getBlock().getType() == Material.COAL_ORE) {
                regenCoal(e.getBlock().getLocation());
                addItem(e.getPlayer(), Material.COAL, 1);
                e.setCancelled(true);
                return;
            }
            if(e.getBlock().getType() == Material.IRON_ORE ){
                regenIron(e.getBlock().getLocation());
                addItem(e.getPlayer(), Material.IRON_ORE, 1);
                e.setCancelled(true);
                return;
            }
            if(e.getBlock().getType() == Material.REDSTONE_ORE ||  e.getBlock().getType() == Material.GLOWING_REDSTONE_ORE ){
                regenRedstone(e.getBlock().getLocation());
                addItem(e.getPlayer(), Material.REDSTONE_ORE, 1);
                e.setCancelled(true);
                return;
            }
            if(e.getBlock().getType() == Material.GOLD_ORE ){
                regenGold(e.getBlock().getLocation());
                addItem(e.getPlayer(), Material.GOLD_ORE, 1);
                e.setCancelled(true);
                return;
            }
            if(e.getBlock().getType() == Material.EMERALD_ORE ){
                if(main.map.getPhase() > 1) {
                    regenEmerald(e.getBlock().getLocation());
                    addItem(e.getPlayer(), Material.EMERALD_ORE, 1);
                    e.setCancelled(true);
                    return;
                } else {
                    e.setCancelled(true);
                    e.getPlayer().sendMessage("§c§l>> §7You can only §c§lBREAK §7this ore in phase 2 and higher!");
                    return;
                }
            }
            if(e.getBlock().getType() == Material.DIAMOND_ORE){
                if(main.map.getPhase() > 2) {
                    regenDiamond(e.getBlock().getLocation());
                    addItem(e.getPlayer(), Material.DIAMOND_ORE, 1);
                    e.setCancelled(true);
                    return;
                } else {
                    e.setCancelled(true);
                    e.getPlayer().sendMessage("§c§l>> §7You can only §c§lBREAK §7this ore in phase 3 and higher!");
                    return;
                }
            }
            if(e.getBlock().getType() == Material.STAINED_CLAY && e.getBlock().getData() == DyeColor.LIME.getData()) {
                e.setCancelled(true);
                e.getPlayer().sendMessage("§c§l>> §7You can only §c§lBREAK §7this ore in phase 2 and higher!");
                return;
            }

            if(e.getBlock().getType() == Material.STAINED_CLAY && e.getBlock().getData() == DyeColor.BLUE.getData()) {
                e.setCancelled(true);
                e.getPlayer().sendMessage("§c§l>> §7You can only §c§lBREAK §7this ore in phase 3 and higher!");
                return;
            }
        }
    }

    public void regenCoal(Location loc) {
        nobreak.add(loc);
        Block b = loc.getBlock();
        b.setType(Material.STAINED_CLAY);
        b.setData(DyeColor.GRAY.getData());
        final Block bl = b;
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                bl.setType(Material.COAL_ORE);
                nobreak.remove(bl.getLocation());
            }
        }, 20*5);
    }

    public void regenIron(Location loc) {
        nobreak.add(loc);
        Block b = loc.getBlock();
        b.setType(Material.STAINED_CLAY);
        b.setData(DyeColor.SILVER.getData());
        final Block bl = b;
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                bl.setType(Material.IRON_ORE);
                nobreak.remove(bl.getLocation());
            }
        }, 20*10);
    }

    public void regenGold(Location loc) {
        nobreak.add(loc);
        Block b = loc.getBlock();
        b.setType(Material.STAINED_CLAY);
        b.setData(DyeColor.YELLOW.getData());
        final Block bl = b;
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                bl.setType(Material.IRON_ORE);
                nobreak.remove(bl.getLocation());
            }
        }, 20*10);
    }

    public void regenRedstone(Location loc) {
        nobreak.add(loc);
        Block b = loc.getBlock();
        b.setType(Material.STAINED_CLAY);
        b.setData(DyeColor.RED.getData());
        final Block bl = b;
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                bl.setType(Material.REDSTONE_ORE);
                nobreak.remove(bl.getLocation());
            }
        }, 20*30);
    }

    public void regenEmerald(Location loc) {
        nobreak.add(loc);
        Block b = loc.getBlock();
        b.setType(Material.STAINED_CLAY);
        b.setData(DyeColor.LIME.getData());
        final Block bl = b;
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                bl.setType(Material.EMERALD_ORE);
                nobreak.remove(bl.getLocation());
            }
        }, 20*30);
    }

    public void regenDiamond(Location loc) {
        nobreak.add(loc);
        Block b = loc.getBlock();
        b.setType(Material.STAINED_CLAY);
        b.setData(DyeColor.BLUE.getData());
        final Block bl = b;
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                bl.setType(Material.IRON_ORE);
                nobreak.remove(bl.getLocation());
            }
        }, 20*40);
    }

    public void addItem(Player p, Material mat, int amount) {
        if(p.getInventory().firstEmpty() == -1) {
            p.getWorld().dropItemNaturally(p.getLocation(), new ItemStack(mat, amount));
        } else {
            p.getInventory().addItem(new ItemStack(mat, amount));
        }
    }
}
