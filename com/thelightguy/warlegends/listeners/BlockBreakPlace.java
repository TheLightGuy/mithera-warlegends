package com.thelightguy.warlegends.listeners;

import com.thelightguy.mith.main.api.API;
import com.thelightguy.mith.main.api.Util;
import com.thelightguy.mith.main.api.packet.ActionBar;
import com.thelightguy.mith.main.api.packet.TitleBar;
import com.thelightguy.mith.main.api.particle.ParticleEffect;
import com.thelightguy.mith.main.api.particle.Trail;
import com.thelightguy.warlegends.main;
import com.thelightguy.warlegends.util.Team;
import com.thelightguy.warlegends.util.TeamType;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.util.Vector;


public class BlockBreakPlace implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if(!main.isStarted) {
            e.setCancelled(true);
            return;
        }
        for(Team t : main.map.getTeams()) {
            if(t.getTeamType() == TeamType.SPECTATOR)  continue;
            int x1 = t.getNexus().getX();
            int z1 = t.getNexus().getZ();
            int x2 = t.getSpawnPoint().getBlockX();
            int z2 = t.getSpawnPoint().getBlockZ();
            Block bl = e.getBlock();
            int blm = main.map.getBuildLimit();
            boolean xb1 = false;
            boolean zb1 = false;
            boolean xb2 = false;
            boolean zb2 = false;
            if(bl.getX() <= (x1+blm) && bl.getX() >= (x1-blm)) {
                xb1 = true;
            }
            if(bl.getZ() <= (z1+blm) && bl.getZ() >= (z1-blm)) {
                zb1 = true;
            }

            if(bl.getX() <= (x2+blm) && bl.getX() >= (x2-blm)) {
                xb2 = true;
            }
            if(bl.getZ() <= (z2+blm) && bl.getZ() >= (z2-blm)) {
                zb2 = true;
            }
            if(xb1 && zb1) {
                e.setCancelled(true);
            }
            if(xb2 && zb2) {
                e.setCancelled(true);
            }
        }
    }



    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        if(!main.isStarted) {
            e.setCancelled(true);
            return;
        }
        for(Team t : main.map.getTeams()) {
            if(t.getTeamType() == TeamType.SPECTATOR) continue;
            if(e.getBlock().getLocation().getBlock().equals(t.getNexus().getLocation().getBlock())) {
                e.setCancelled(true);
                if(t.getPlayers().contains(e.getPlayer().getUniqueId())) {
                    ActionBar actionBar = new ActionBar("§cYou can't destroy your own nexus");
                    actionBar.sendToPlayer(e.getPlayer());
                    e.getPlayer().playSound(e.getBlock().getLocation(), Sound.ENDERMAN_TELEPORT, 1, 1);
                    return;
                }
                if(t.getNexusHealth() == 1) {
                    t.setNexusHealth(0);
                    t.destroyNexus();
                    e.getBlock().setType(Material.AIR);
                    e.getBlock().getWorld().createExplosion(t.getNexusLocaiton(), 3, true);
                    e.getBlock().getWorld().playSound(e.getBlock().getLocation(), Sound.ENDERDRAGON_HIT, 2, 1);
                    e.getBlock().getWorld().playSound(e.getBlock().getLocation(), Sound.ENDERDRAGON_GROWL, 2, 1);
                    e.getBlock().getWorld().playSound(e.getBlock().getLocation(), Sound.BLAZE_DEATH, 2, 1);
                    for(int i = 0; i < 21; i++) {
                        int r = t.getColor().getColor().getRed();
                        int g = t.getColor().getColor().getGreen();
                        int b = t.getColor().getColor().getBlue();
                        int rn = Util.randomWithRange(10, 20);
                        Location loc = new Location(t.getNexusLocaiton().getWorld(),
                                t.getNexusLocaiton().getX(),
                                t.getNexusLocaiton().getY(),
                                t.getNexusLocaiton().getZ());

                        Location loc2 = new Location(t.getNexusLocaiton().getWorld(), Util.randomWithRange(-100, 100), Util.randomWithRange(-100, 100), Util.randomWithRange(-100, 100));
                        loc2.add(loc.getX(), loc.getY(), loc.getZ());
                        Trail trail = new Trail(e.getBlock().getLocation(), loc.toVector().subtract(loc2.toVector()).normalize(), t.getColor(), 15);
                        trail.run();
                    }

                    for(Player p : Bukkit.getOnlinePlayers()) {
                        if(t.getPlayers().contains(p.getUniqueId())) {
                            TitleBar title = new TitleBar("§cNEXUS DESTROYED", "You won't respawn", 20, 30);
                            title.sendToPlayer(p);
                        } else {
                            TitleBar title = new TitleBar(t.getChatColor()+t.getName().toUpperCase()+" DESTROYED", "", 20, 30);
                            title.sendToPlayer(p);
                        }
                    }
                } else {
                    if(t.getNexusHealth() == 0) {
                        e.getBlock().setType(Material.AIR);
                        return;
                    }
                    e.getBlock().getWorld().playSound(e.getBlock().getLocation(), Sound.ANVIL_LAND, 2, 1);
                    ParticleEffect.SMOKE_LARGE.display(0,0,0,0.12f,10,t.getNexusLocaiton(), 100);
                    t.setNexusHealth(t.getNexusHealth()-1);

                }
            } else {
                int x1 = t.getNexus().getX();
                int z1 = t.getNexus().getZ();
                int x2 = t.getSpawnPoint().getBlockX();
                int z2 = t.getSpawnPoint().getBlockZ();
                Block bl = e.getBlock();
                int blm = main.map.getBuildLimit();
                boolean xb1 = false;
                boolean zb1 = false;
                boolean xb2 = false;
                boolean zb2 = false;
                if(bl.getX() <= (x1+blm) && bl.getX() >= (x1-blm)) {
                    xb1 = true;
                }
                if(bl.getZ() <= (z1+blm) && bl.getZ() >= (z1-blm)) {
                    zb1 = true;
                }

                if(bl.getX() <= (x2+blm) && bl.getX() >= (x2-blm)) {
                    xb2 = true;
                }
                if(bl.getZ() <= (z2+blm) && bl.getZ() >= (z2-blm)) {
                    zb2 = true;
                }
                if(xb1 && zb1) {
                    e.setCancelled(true);
                }
                if(xb2 && zb2) {
                    e.setCancelled(true);
                }
            }
        }
    }
}
