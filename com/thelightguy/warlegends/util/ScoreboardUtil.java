package com.thelightguy.warlegends.util;

import com.thelightguy.mith.main.api.API;
import com.thelightguy.mith.main.api.Util;
import com.thelightguy.warlegends.main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ScoreboardUtil {

    public synchronized static void run(Player p) {
        if(!main.isStarted) {
            runLobby(p);
            return;
        }
        Date d = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy");
        String date = dt.format(d);

        List<String> scoreboard = new ArrayList<>();
        scoreboard.add("§7"+date);
        scoreboard.add("§7 "); //space
        scoreboard.add("Phase");
        scoreboard.add("§7"+main.map.getPhase());
        scoreboard.add("  "); //space
        for(Team t : main.map.getTeams()) {
            if(t.getTeamType() == TeamType.SPECTATOR) continue;
            String h = t.getNexusHealth()+"";
            if(t.getNexusHealth() < 6) h="§4§l"+h;
            if(t.getNexusHealth() < 11) h="§4"+h;
            if(t.getNexusHealth() < 26) h="§c"+h;
            if(t.getNexusHealth() < 51) h="§e"+h;
            if(t.getNexusHealth() <= 0 && t.getPlayers().size() > 1) h="§f"+t.getPlayers().size()+" players";
            if(t.getNexusHealth() <= 0 && t.getPlayers().size() == 1) h="§f"+t.getPlayers().size()+" player";
            if(t.getNexusHealth() == 0 && t.getPlayers().size() == 0) h = "§c§lDEAD";
            String n = t.getChatColor() + t.getName() + " §7Nexus "+h;
            scoreboard.add(n);
        }
        scoreboard.add("   "); //space
        scoreboard.add("§cplay.mithera.net");
        API.objectiveName = "§c§lWARLEGENDS";
        API.scoreboards.remove(p);
        API.scoreboards.put(p, scoreboard);
    }

    public static void runLobby(Player p) {
        if(main.isStarted) {
            run(p);
            return;
        }
        Date d = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("MM/dd/yyyy");
        String date = dt.format(d);

        List<String> scoreboard = new ArrayList<>();
        scoreboard.add("§7"+date);
        scoreboard.add("§7 ");
        scoreboard.add("Players: §7" + Bukkit.getOnlinePlayers().size());
        scoreboard.add(" ");
        scoreboard.add("Kit: §7DEFAULT");
        scoreboard.add("  ");
        scoreboard.add("Starting in:");
        scoreboard.add("§7"+Game.timer+" seconds");
        scoreboard.add("   ");
        scoreboard.add("§cplay.mithera.net");
        API.objectiveName = "§c§lWARLEGENDS";
        API.scoreboards.remove(p);
        API.scoreboards.put(p, scoreboard);
    }

}
