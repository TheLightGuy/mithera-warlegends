package com.thelightguy.warlegends.util;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.thelightguy.mith.main.api.API;
import com.thelightguy.mith.main.api.packet.TitleBar;
import com.thelightguy.mith.main.api.player.MithPlayer;
import com.thelightguy.mith.main.api.particle.ParticleEffect;
import com.thelightguy.mith.main.api.particle.Trail;
import com.thelightguy.warlegends.listeners.NexusDefense;
import com.thelightguy.warlegends.main;
import com.thelightguy.warlegends.maps.Map;
import com.thelightguy.warlegends.maps.MapType;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Random;
import java.util.UUID;

import static com.thelightguy.warlegends.main.isStarted;
import static com.thelightguy.warlegends.main.plugin;

public class Game {

    public static boolean runtimer = false;
    public static int timer = 60;

    public static void start() {
        if (main.isStarted) return;
        if (main.map == null) main.map = new Map(MapType.DEFAULT);
        main.isStarted = true;
        API.overWrideTabList = true;
        API.overWridecustomNamePrefix = true;
        Bukkit.broadcastMessage("Game start");
        int i = 1;
        particles();
        for (Player p : Bukkit.getOnlinePlayers()) {
            Team t = main.map.getTeams().get(i);
            t.addPlayer(p);
            p.teleport(t.getSpawnPoint());
            i++;
        }
        loop();
        nexusDefense(0);
        NexusDefense.handle();
        main.map.nextPhase();
    }

    public static void close() {
        Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
            @Override
            public void run() {
                main.isEnded = false;
                for (Player p : Bukkit.getOnlinePlayers()) {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("Connect");
                    out.writeUTF("WLL");
                    p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
                }
                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            p.kickPlayer("Game ended");
                        }
                    }
                },20*3);
                Bukkit.unloadWorld(main.map.getWorld(), false);
                Bukkit.getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        Bukkit.shutdown();
                    }
                }, 20 * 10);
            }
        }, 20 * 3);

    }
    public static void stop() {
        if (!main.isStarted) return;
        API.customNamePrefix.clear();
        API.customTabListName.clear();
        API.overWrideTabList = false;
        API.overWridecustomNamePrefix = false;
        main.isStarted = false;
        main.map = null;
    }

    public static synchronized void timer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (isStarted) cancel();
                int p = Bukkit.getOnlinePlayers().size();
                int sp = main.map.getTeams().get(0).getPlayers().size();
                int v = API.vanishedPlayers;
                int pls = p - sp - v;
                if (pls >= main.map.getMinplayers()) {
                    runtimer = true;
                }
                if (runtimer) {
                    if (pls < main.map.getMinplayers()) {
                        runtimer = false;
                    } else {
                        String s = "";
                        if (timer == 0) {
                            s = "§aGO";
                            runtimer = false;
                            start();
                            cancel();
                        } else if (timer == 1) {
                            s = "§c§l1";
                        } else if (timer < 3) {
                            s = "§e§l" + timer;
                        } else if (timer < 6) {
                            s = "§a§l" + timer;
                        } else if (timer == 10) {
                            s = "§a§l" + timer;
                        } else if (timer == 15) {
                            s = "§a§l" + timer;
                        } else if (timer == 30) {
                            s = "§a§l" + timer;
                        } else if (timer == 60) {
                            s = "§a§l" + timer;
                        }
                        if (!s.equals("")) {
                            for (Player pl : Bukkit.getOnlinePlayers()) {
                                TitleBar titleBar = new TitleBar(s, "", 40, 0);
                                titleBar.sendToPlayer(pl);
                                pl.playSound(pl.getLocation(), Sound.CLICK, 1, 1);
                            }
                            if (timer != 0) {
                                if (timer == 1) {
                                    Bukkit.broadcastMessage("§c§l>> §7The game starts in §c§l" + timer + " §7second!");
                                } else {
                                    Bukkit.broadcastMessage("§c§l>> §7The game starts in §c§l" + timer + " §7seconds!");
                                }
                            }
                        }
                        timer--;
                    }
                }
            }
        }
                .runTaskTimer(plugin, 0, 20);
    }

    public static boolean checkTeam() {
        int teamsalive = 0;
        Team lasteam = main.map.getTeams().get(0);
        for (Team t : main.map.getTeams()) {
            if (t.getTeamType() == TeamType.SPECTATOR) continue;
            if (t.getPlayers().size() > 0) {
                teamsalive++;
                lasteam = t;
                Bukkit.broadcastMessage(teamsalive+"");
            }
        }
        if (teamsalive <= 1) {
            Bukkit.broadcastMessage("§a§lTeam " + lasteam.getName() + "§a won the game");
            close();
            return true;
        } else {
            return false;
        }
    }


    public static synchronized void loop() {
        Bukkit.getScheduler().scheduleAsyncRepeatingTask(plugin, new Runnable() {
            @Override
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()) {
                    if(main.isStarted) {
                        ScoreboardUtil.run(p);
                    } else {
                        ScoreboardUtil.runLobby(p);
                    }
                }

            }
        },0,20);
    }

    public static synchronized void loopCloud() {
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()) {
                    if(p.isFlying()) {
                        if(API.vanishedPlayerList.contains(p.getUniqueId())) continue;
                        Location loc = p.getLocation();
                        loc.setY(loc.getY()-0.1);
                        ParticleEffect.CLOUD.display(0.22F, 0.24F, 0.22F, 0.007F, 4, p.getLocation(), 300);
                    }
                }
                if(!main.isStarted) loopCloud();
            }
        },1);
    }

    public static synchronized void nexusDefense(int tick) {
        final int t = tick+1;
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                if(!main.isStarted) return;
                for(Team t : main.map.getTeams()) {
                    if(t.isNexusDead()) continue;
                    if(t.getTeamType() == TeamType.SPECTATOR) continue;
                    Location loc = new Location(t.getNexusLocaiton().getWorld(),
                            t.getNexusLocaiton().getX(),
                            t.getNexusLocaiton().getY()+2,
                            t.getNexusLocaiton().getZ());
                    Fireball arrow = (Fireball) t.getNexus().getWorld().spawnEntity(loc, EntityType.SMALL_FIREBALL);
                    arrow.setIsIncendiary(false);
                    boolean found = false;
                    Player p = Bukkit.getPlayer(UUID.randomUUID());
                    for(Entity en : arrow.getNearbyEntities(10,11,10)) {
                        if(en instanceof Player) {
                            Player pl= (Player) en;
                            MithPlayer mp = new MithPlayer(pl);
                            if(mp.isVanish()) continue;
                            if(t.getPlayers().contains(pl.getUniqueId())) continue;
                            if(pl.getGameMode()  == GameMode.SPECTATOR) continue;
                            p= (Player) en;
                            found = true;
                        }
                    }
                    if(!found) {
                        arrow.remove();
                    } else {
                        Location lo = p.getLocation();
                        lo.setY(lo.getY()+1);
                        arrow.setDirection(lo.toVector().subtract(arrow.getLocation().toVector()).normalize());
                        double reach = p.getLocation().distance(arrow.getLocation());
                        Trail trail = new Trail(arrow.getLocation(), arrow.getDirection(), t.getColor(), (int) reach);
                        trail.run();
                        arrow.setVelocity(arrow.getVelocity().multiply(0.001));
                        found = true;
                        Location nl = new Location(t.getNexusLocaiton().getWorld(), t.getNexusLocaiton().getX(), t.getNexusLocaiton().getY(), t.getNexusLocaiton().getZ());
                        nl.setY(nl.getY()+2);
                    }
                    NexusDefense.arrows.put(arrow,t);
                    NexusDefense.arrowList.add(arrow);
                }
                nexusDefense(t);
            }
        }, 10);
    }

    public static synchronized void particles() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            @Override
            public void run() {
                for(Team t : main.map.getTeams()) {
                    if(t.getTeamType() == TeamType.SPECTATOR) continue;
                    if(t.getNexusHealth() <= 0) continue;
                    Location loc = t.getNexusLocaiton();
                    int speed = 25+(-t.getNexusHealth()/3)+1;
                    ParticleEffect.ENCHANTMENT_TABLE.display(0,1,0,speed,20+(speed*2),loc, 15);
                    Location loc2 = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
                    DyeColor color = t.getColor();
                    for(int i = 0; i< 20; i++) {
                        Random random = new Random();
                        loc2.setX(getrandomnumber(0, 50)+loc2.getX());
                        loc2.setY(getrandomnumber(0, 50)+loc2.getY());
                        loc2.setZ(getrandomnumber(0, 50)+loc2.getZ());
                        int r = color.getColor().getRed();
                        int g = color.getColor().getGreen();
                        int b = color.getColor().getBlue();
                        ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(r,g,b), loc2, 100);
                    }
                }
            }
        },0, 2);
    }

    public static double getrandomnumber(int min, int max) {
        Random rand = new Random();
        int r = rand.nextInt((max - min) + 1) + min;
        double rn = r/((max-min)*2);
        return rn;
    }

}
