package com.thelightguy.warlegends.util;

import com.thelightguy.mith.main.api.API;
import com.thelightguy.mith.main.sql.PlayerSQL;
import com.thelightguy.warlegends.main;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Team {

    private Location spawnPoint;
    private Location nexusLocaiton;
    private boolean isNexusDead;
    private List<UUID> players;
    private List<UUID> offlinePlayers;
    private Block nexus;
    private int nexusHealth;
    private String name;
    private String icon;
    private DyeColor color;
    private ChatColor chatColor;
    private TeamType teamType;


    public Team(Location spawnPoint, List<UUID> players, Block nexus, int nexusHealth, TeamType teamType, String name, String icon, DyeColor color, ChatColor chatColor) {
        if(spawnPoint != null) this.spawnPoint = spawnPoint;
        this.players = players;
        if(nexus != null) this.nexus = nexus;
        if(nexusHealth != 0) this.nexusHealth = nexusHealth;
        this.name = name;
        this.icon = icon;
        this.color = color;
        this.teamType = teamType;
        this.offlinePlayers = new ArrayList<UUID>();
        if(nexus != null) this.isNexusDead = false;
        if(nexus != null) this.nexusLocaiton = nexus.getLocation();
        this.chatColor = chatColor;
        if(nexus != null) {
            Location loc = this.nexusLocaiton;
            loc.setX(loc.getX()+0.5);
            loc.setY(loc.getY()+0.5);
            loc.setZ(loc.getZ()+0.5);
            nexus.setType(Material.STAINED_GLASS);
            nexus.setData(color.getData());
            this.nexusLocaiton=loc;
        }
    }
    public void spawnPlayer(Player p) {
        if(!players.contains(p.getUniqueId())) return;
        if(teamType == TeamType.SPECTATOR) {
            p.setGameMode(GameMode.SPECTATOR);
        } else {
            p.setGameMode(GameMode.SURVIVAL);
        }
        p.teleport(spawnPoint);
    }
    public boolean isNexusDead() {
        if(teamType == TeamType.SPECTATOR) return false;
        if(nexusHealth <= 0) this.isNexusDead = true;
        return isNexusDead;
    }
    public Location getSpawnPoint() {
        return spawnPoint;
    }
    public List<UUID> getPlayers() {
        return players;
    }
    public void addPlayer(Player p) {
        for(Team t : main.map.getTeams()) {
            if(t.getPlayers().contains(p.getUniqueId())) {
                t.removePlayer(p);
            }
        }
        if(API.customNamePrefix.containsKey(p)) {
            API.customNamePrefix.remove(p);
        }
        if(API.customTabListName.containsKey(p)) {
            API.customTabListName.remove(p);
        }
        API.customNamePrefix.put(p, chatColor+"");
        API.customTabListName.put(p, "§7|| "+chatColor+"§l"+icon+" §7|| ");
        players.add(p.getUniqueId());
        if(teamType == TeamType.SPECTATOR) {
            p.setGameMode(GameMode.SPECTATOR);
            p.teleport(spawnPoint);
        }
    }
    public void addOfflinePlayer(Player p) {
        offlinePlayers.add(p.getUniqueId());
        players.remove(p.getUniqueId());
    }
    public void removeOfflinePlayer(Player p) {
        offlinePlayers.add(p.getUniqueId());
        players.remove(p.getUniqueId());
    }
    public void removePlayer (Player p) {
        offlinePlayers.add(p.getUniqueId());
        players.remove(p.getUniqueId());
    }
    public List<UUID> getOfflinePlayers() {
        return offlinePlayers;
    }
    public Location getNexusLocaiton() {
        return nexusLocaiton;
    }
    public Block getNexus() {
        return nexus;
    }
    public void destroyNexus() {
        this.offlinePlayers.clear();
    }
    public int getNexusHealth() {
        return nexusHealth;
    }
    public void setNexusHealth(int nexusHealth) {
        this.nexusHealth = nexusHealth;
    }
    public String getName() {
        return name;
    }
    public String getIcon() {
        return icon;
    }
    public DyeColor getColor() {
        return color;
    }
    public ChatColor getChatColor() {
        return chatColor;
    }
    public TeamType getTeamType() {
        return teamType;
    }
}
